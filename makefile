FLAGS = 
OBJS = main.o builtin.o helper.o basicIO.o procrel.o
TARGET = cocosh
COMPILER = gcc

.PHONY: clean pack

all: $(TARGET)

%.o: src/%.c src/%.h src/include.h
	$(COMPILER) -c $< $(FLAGS)

$(TARGET): $(OBJS)
	$(COMPILER) -g $^ -o $@ $(FLAGS)

clean:
	-rm $(OBJS) $(TARGET)

pack: all
	-rm $(OBJS)
