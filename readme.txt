1)Use 'make' command to build the shell
2)Launch the cocoShell using the executable created : ./cocosh
3)Use double quotes to include spaces in arguments
4) Use single quotes to take the literal string
5)Add & at the end to run the process in background
6)Use > and < for output and input redirection respectively
7)Use | to pipe the output of commands
8)Use 'jobs' to list the current jobs
9)Use kjob index signal to send signal to process with index
10)Use C-z and C-c to stop and kill the process respectively
