#include "basicIO.h"

int bIO_readline(char *ar,int arsize){
	int i,j;
	for(i=0;i<arsize-1;i++){
		j=read(0,ar+i,1);
		if(j==0){
			ar[i]='\0';
			return 0;
		}
		else if(j!=1){
			ar[i+1]='\0';
			return j;
		}
		if(ar[i]=='\n'||ar[i]=='\0'){
			break;
		}
	}
	while(read(0,ar+i,1)>=0);
	ar[i]='\0';
	return 0;
}
