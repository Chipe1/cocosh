#include "builtin.h"

//returns path relative to home directory
char *pwd_rel(void){
	//temporary string/string to return
	char *cwd,*toret;
	cwd=(char *)malloc(sizeof(char)*STRSIZE);
	if(getcwd(cwd,STRSIZE)==NULL){
		perror("path too large");
		exit(-1);
	}
	//cwd contains the absolute path
	//if cwd starts with home directory,print ~
	if(hDir!=NULL&&strncmp(hDir,cwd,strlen(hDir))==0){
		//allocate memory to return string
		toret=(char *)malloc(sizeof(char)*STRSIZE);
		//format the return string
		sprintf(toret,"~%s",cwd+strlen(hDir));
		//free the temporary string
		free(cwd);
		return toret;
	}
	else{
		//return the whole string 
		return cwd;
	}
}

//returns the absolute path of the current directory
char *pwd_abs(void){
	char *cwd;
	cwd=(char *)malloc(sizeof(char)*STRSIZE);
	if(getcwd(cwd,STRSIZE)==NULL){
		perror("path too large");
		exit(-1);
	}
	return cwd;
}

//changes the current directory of the process
int changeDir(const char *path){
	char *n_path;
	int i,j,inquote;

	//If no path is given, go to home directory
	if(path==NULL||*path=='\0'){
		return chdir(hDir);
	}

	n_path=(char *)malloc(sizeof(char)*STRSIZE);
	n_path[STRSIZE-1]='\0';
	for(i=0,j=0,inquote=0;path[i]!='\0'&&j<STRSIZE;i++){
		//string ends at white character
		if(inquote==0&&(path[i]==' '||path[i]=='\t')){
			break;
		}
		//escape character found
		else if(inquote==0&&path[i]=='\\'){
			n_path[j++]=path[++i];
		}
		//if single/double quotes are found,keep reading until the quotes are closed
		else if(inquote!=0){
			//quotes are closed
			if((path[i]=='\''&&inquote==1)||(path[i]=='\"'&&inquote==2)){
				inquote=0;
			}
			//text in quote
			else{
				n_path[j++]=path[i];
			}
		}
		//not in quotes,character is normal(no white space/escape character)
		else{
			//single quote starts
			if(path[i]=='\'')
				inquote=1;
			//double quote starts
			else if(path[i]=='\"')
				inquote=2;
			else
				n_path[j++]=path[i];
		}
	}
	if(j==STRSIZE){
		fprintf(stderr,"Path \033\[30m%s\033\[m is too large:\tConverted improperly",path);
		j--;
	}
	n_path[j]='\0';
	
	//translate ~ to homedirectory
	if(n_path[0]=='~'){
		chdir(hDir);
		n_path[0]='.';
	}
	i=chdir(n_path);
	//free the allocated memory
	free(n_path);
	return i;
}

void parrot(char * const *text){
	int i;
	if(text==NULL||*text==NULL)
		return ;
	for(i=1;text[i]!=NULL;i++)
		printf("%s ",text[i]);
	printf("\n");
}
