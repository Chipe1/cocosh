#include "include.h"
#include "helper.h"

//return the path of the current directory
char *pwd_rel(void);
char *pwd_abs(void);
int changeDir(const char *);
void parrot(char * const *);
