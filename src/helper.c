#include "helper.h"

//Prints the prompt before each command
void printPrompt(void){
	char *relPath;
	relPath=pwd_rel();
	printf("\033\[1;32m%s@%s\033\[m \033\[1;34m%s $CocoShell$ "STR_NORM,uName,mName,relPath);
	free(relPath);
}


//Strips the input off extra blank spaces
//NOTE : The user must free both the input and output strings
char *strip(const char *input){
	int i,j;
	char *toret;
	//validate input
	if(input==NULL)
		return NULL;
	if(strlen(input)>=STRSIZE){
		fprintf(stderr,STR_RED"ERROR :"STR_NORM" input string \""STR_BLK"%s"STR_NORM"\" too large to strip\n",input);
	}
	toret=(char *)malloc(sizeof(char)*STRSIZE);
	//strip the start
	for(i=0;input[i]!='\0'&&instring(input[i]," \t\n");i++);
	for(j=0;input[i]!='\0';i++,j++)
		toret[j]=input[i];
	toret[STRSIZE-1]='\0';
	toret[j]='\0';
	j--;
	while(j>=0&&instring(toret[j]," \t\n"))
		toret[j--]='\0';
	return toret;
}


char *parseStr(const char *inp){
	char *toret;
	int i,j,inquote,justwhite;
	
	toret=(char *)malloc(sizeof(char)*STRSIZE);
	toret[STRSIZE-1]='\0';
	for(i=0,j=0,justwhite=1,inquote=0;inp[i]!='\0'&&j<STRSIZE;i++){
		//escape character found
		if(inquote!=1&&inp[i]=='\\'){
			toret[j++]=inp[++i];
			justwhite=0;
		}
		//if single/double quotes are found,keep reading until the quotes are closed
		else if(inquote!=0){
			//quotes are closed
			if((inp[i]=='\''&&inquote==1)||(inp[i]=='\"'&&inquote==2)){
				inquote=0;
			}
			//text in quote
			else{
				toret[j++]=inp[i];
			}
			justwhite=0;
		}
		//not in quotes
		else{
			//single quote starts
			if(inp[i]=='\'')
				inquote=1;
			//double quote starts
			else if(inp[i]=='\"')
				inquote=2;
			//white space character is found
			else if(instring(inp[i]," \t\n")){
				if(!justwhite){
					toret[j++]=inp[i];
					justwhite=1;
				}
			}
			//normal character is found
			else{
					toret[j++]=inp[i];
					justwhite=0;
			}
		}
	}
	if(j==STRSIZE){
		fprintf(stderr,"Inp "STR_BLK"%s"STR_NORM" is too large:\tConverted improperly",inp);
		j--;
	}
	toret[j]='\0';
	return toret;
}

char **tokenize(const char *str,const char *delim,int parse){
	char **toret;
	int c,i,j,inquote;
	/*
	  inquote = 1 if in ' quotes
	  inquote = 2 if in " quotes
	 */
	if(str==NULL)
		return NULL;
	toret=(char **)malloc(sizeof(char *)*101);
	for(i=0;i<101;i++)
		toret[i]=NULL;
	toret[0]=(char *)malloc(sizeof(char)*STRSIZE);

	//evaluate the string if parse is non-zero
	for(c=0,i=0,j=0,inquote=0;str[i]!='\0'&&j<STRSIZE;i++){
		//string is split at  white characters
		if(inquote==0&&instring(str[i],delim)){
			//check for double spaces
			if(j!=0){
				toret[c][j]='\0';
				c++;
				j=0;
				toret[c]=(char *)malloc(sizeof(char)*STRSIZE);
			}
			continue;
		}
		//escape character found
		else if(inquote!=1&&str[i]=='\\'){
			if(!parse)
				toret[c][j++]=str[i];
			toret[c][j++]=str[++i];
		}
		//if single/double quotes are found,keep reading until the quotes are closed
		else if(inquote!=0){
			//quotes are closed
			if((str[i]=='\''&&inquote==1)||(str[i]=='\"'&&inquote==2)){
				inquote=0;
				if(!parse)
					toret[c][j++]=str[i];
			}
			//text in quote
			else{
				toret[c][j++]=str[i];
			}
		}
		//not in quotes,character is normal(no white space/escape character)
		else{
			//single quote starts
			if(str[i]=='\''){
				inquote=1;
				if(!parse)
					toret[c][j++]=str[i];

			}
			//double quote starts
			else if(str[i]=='\"'){
				inquote=2;
				if(!parse)
					toret[c][j++]=str[i];
			}
			else
				toret[c][j++]=str[i];
		}
	}
	if(c>=100){
		fprintf(stderr,"Could'nt tokenize");
		c--;
	}
	if(j>=STRSIZE){
		fprintf(stderr,"token too large to tokenize");
		j--;
	}
	//assign NULL to the last token
	if(j!=0)
		toret[c][j]='\0';
	else{
		free(toret[c]);
		toret[c]=NULL;
	}
	return toret;
}

void freetoken(char **toFree){
	int i;
	//check input
	if(toFree==NULL)
		return ;
	i=0;
	//free the strings of the arglist
	while(toFree[i]!=NULL){
		free(toFree[i]);
		i++;
	}
	//free the arglist
	free(toFree);
}

int instring(const char c,const char *s){
	int i;
	for(i=0;s[i]!='\0';i++)
		if(c==s[i])
			return 1;
	return 0;
}

int runit(const char *cmd,int inp_fd,int out_fd){
	int fd,i,inbg,status;
	char **arglist,**redirlist,*tempstr_1,*toexec,*strpcmd;
	pid_t cpid;

	if(cmd==NULL||*cmd=='\0'){
		return 0;
	}

	//check for &	
	strpcmd=strip(cmd);
	if(strpcmd==NULL||*strpcmd=='\0'){
		free(strpcmd);
		return 0;
	}
	if(strpcmd[strlen(strpcmd)-1]=='&'){
		strpcmd[strlen(strpcmd)-1]='\0';
		inbg=1;
	}
	else
		inbg=0;

	//manage output redirection
	redirlist=tokenize(strpcmd,">",0);
	//validate output redirection
	if(redirlist==NULL||*redirlist==NULL||redirlist[2]!=NULL){
		freetoken(redirlist);
		fprintf(stderr,STR_RED"ERROR :"STR_NORM"improper redirection of \""STR_BLK"%s"STR_NORM"\"\n",strpcmd);
	}
	toexec=redirlist[0];
	//replace the output if redirection is required
	if(redirlist[1]!=NULL){
		tempstr_1=parseStr(redirlist[1]);
		//		printf("printing into %s\n",tempstr_1);
		i=strlen(toexec);
		//open with append
		if(strpcmd[i]=='>'&&strpcmd[i+1]=='>'){
			fd=open(tempstr_1,O_CREAT|O_WRONLY|O_APPEND,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
		}
		//open with truncate
		else{
			fd=open(tempstr_1,O_CREAT|O_WRONLY|O_TRUNC,S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
		}
		if(fd<0){
			perror(STR_RED"Error :"STR_NORM" Couldn't open/create file for writing ");
			fd=1;
		}
		if(out_fd!=1)
			close(out_fd);
		out_fd=fd;
		free(tempstr_1);
	}
	//protect the originl toexec=redirlist[0] from freeing
	redirlist[0]=(char *)malloc(sizeof(char)*2);
	//clean the arglist
	freetoken(redirlist);

	//manage input redirection
	redirlist=tokenize(toexec,"<",0);
	if(redirlist==NULL||*redirlist==NULL||redirlist[2]!=NULL){
		freetoken(redirlist);
		free(toexec);
		fprintf(stderr,STR_RED"ERROR :"STR_NORM"improper redirection of \""STR_BLK"%s"STR_NORM"\"\n",strpcmd);		
	}
	free(strpcmd);
	free(toexec);
	toexec=redirlist[0];
	//replace the input if redirection is required
	if(redirlist[1]!=NULL){
		tempstr_1=parseStr(redirlist[1]);
		//		printf("reading from %s\n",tempstr_1);
		fd=open(tempstr_1,O_RDONLY,0);
		if(fd<0){
			perror(STR_RED"Error :"STR_NORM" Couldn't open file for reading ");
			fd=0;
		}
		if(inp_fd!=0)
			close(inp_fd);
		inp_fd=fd;
		free(tempstr_1);
	}
	//protect the originl toexec=redirlist[0] from freeing
	redirlist[0]=(char *)malloc(sizeof(char)*2);
	//clean the arglist
	freetoken(redirlist);

	//tokenize the command by spaces
	arglist=tokenize(toexec," \t",1);
	//free toexec
	free(toexec);
	
	//If argument list is empty, skip to cleaning
	if(arglist==NULL||*arglist==NULL){;}

	//exit command is typed
	else if(strcmp(*arglist,"exit")==0||strcmp(*arglist,"quit")==0){
		exit(0);
	}
	//cd is typed
	else if(strcmp(*arglist,"cd")==0){
		if(changeDir(arglist[1])!=0){
			perror(STR_RED"Error changing directory "STR_NORM);
			return -1;
		}
	}
	//kjob is typed
	else if(strcmp(*arglist,"kjob")==0){
		if(arglist[1]==NULL||arglist[2]==NULL||arglist[3]!=NULL){
			fprintf(stderr,STR_BLU"Usage :"STR_CYN" kjob"STR_YEL" id signal"STR_NORM"\n");
			return -1;
		}
		if(cocoatoi(arglist[1])<=0||cocoatoi(arglist[2])<=0){
			fprintf(stderr,STR_RED"Error :"STR_NORM" id and signal must be positive integers\n");
			return -1;
		}
		if(kjob(cocoatoi(arglist[1]),cocoatoi(arglist[2]))!=cocoatoi(arglist[1]))
			return -1;
		else
			return 0;
	}
	//overkill is typed
	else if(strcmp(*arglist,"overkill")==0){
		if(arglist[1]!=NULL)
			printf(STR_GRN"Note :"STR_CYN" overkill"STR_NORM" takes no arguments\n");
		overkill();
	}
	//fg is typed
	else if(strcmp(*arglist,"fg")==0){
		if(arglist[1]!=NULL&&arglist[2]!=NULL){
			fprintf(stderr,STR_BLU"Usage :"STR_CYN" fg"STR_YEL" id"STR_NORM"(optional)\n");
			return -1;
		}
		if(arglist[1]==NULL){
			if(procstack==NULL){
				printf("No process in background");
				return 0;
			}
			else{
				i=procstack->pinfo->index;
				if(fg(i)!=i)
					return -1;
				else return 0;
			}
		}
		else{
			if(cocoatoi(arglist[1])<0){
				fprintf(stderr,STR_RED"Error :"STR_NORM" Argument "STR_BLK"%s"" is not a valid id\n",arglist[1]);
				return -1;
			}
			if(fg(cocoatoi(arglist[1]))!=cocoatoi(arglist[1]))
				return -1;
			return 0;
		}
	}
	//requires spawning of new process (or) output
	else{
		cpid=fork();
		if(cpid<0){
			fprintf(stderr,STR_RED"ERROR : "STR_NORM"Could'nt create a new process\n");
			return -1;
		}
		//child process
		else if(cpid==0){
			//handle signals
			/* if (signal(SIGTSTP, sig_handler) == SIG_ERR) */
			/* 	fprintf(stderr,"\n"STR_RED"Error :"STR_NORM"can't catch "STR_BLU"SIGTSTP"STR_NORM"\n"); */

			//change input stream
			if(inp_fd!=0){
				dup2(inp_fd,0);
				close(inp_fd);
			}
			//change output stream
			if(out_fd!=1){
				dup2(out_fd,1);
				close(out_fd);
			}
			
			//pwd command is typed
			if(strcmp(*arglist,"pwd")==0){
				if(arglist[1]!=NULL){
					printf(STR_GRN"Note :"STR_CYN" pwd"STR_NORM" takes no arguments\n");
				}
				tempstr_1=pwd_abs();
				printf("%s\n",tempstr_1);
				//free the memory allocated by pwd_abs
				free(tempstr_1);
			}
			else if(strcmp(*arglist,"jobs")==0){
				if(arglist[1]!=NULL){
					printf(STR_GRN"Note :"STR_CYN" jobs"STR_NORM" takes no arguments\n");
				}
				jobs(procstack);
			}
			//echo is typed
			else if(strcmp(*arglist,"echo")==0){
				parrot(arglist);
			}
			//other user command is typed
			else{
				if(execvp(*arglist,arglist)<0){
					perror("\033\[1;31mError "STR_NORM);
				}
			}
			//Safely quit the child process
			_exit(0);
		}
		//parent process
		else{
			if(!inbg){
				infg=addProc(STAT_FR,cpid,*arglist);
				waitpid(cpid,&status,WUNTRACED);
				if(WIFSTOPPED(status)){
					changeProcStat(infg,STAT_BS);
				}
				else{
					changeProcStat(infg,STAT_K);
				}
				infg=0;
			}
			else{
				addProc(STAT_BR,cpid,*arglist);
			}
		}
	}
	//free the arglist
	freetoken(arglist);
	return 0;
}

int cocoatoi(const char *s){
	int toret,i;
	for(i=0,toret=0;s[i]!='\0';i++){
		if(i>9||s[i]<'0'||s[i]>'9'){
			return -1;
		}
		else{
			toret=10*toret+s[i]-'0';
		}
	}
	return toret;
}
