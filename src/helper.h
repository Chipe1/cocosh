#include "include.h"
#include "procrel.h"

extern char *pwd_rel(void);
extern char *pwd_abs(void);
extern void child_sig_handler(int);
extern int changeDir(const char *);
extern void parrot(char * const *);

void printPrompt(void);
char *strip(const char *);
char *parseStr(const char *);
char **tokenize(const char *,const char *,int);
void freetoken(char **);
int instring(const char,const char *);
int runit(const char *,int,int);
int cocoatoi(const char *);
