#ifndef __coconutshell__
#define __coconutshell__

//include libraries
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<pwd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<signal.h>

//default size of input/output string
#define STRSIZE 256

//string variables
#define STR_NORM "\033\[0m"
#define STR_RED "\033\[31m"
#define STR_GRN "\033\[32m"
#define STR_YEL "\033\[33m"
#define STR_BLU "\033\[34m"
#define STR_MAG "\033\[35m"
#define STR_CYN "\033\[36m"
#define STR_WHT "\033\[37m"
#define STR_BLK "\033\[30m"

//structure to hold child info
typedef struct ProcInfo{
	int index,status;
	pid_t pid;
	char *name;
}ProcInfo;

typedef struct ProcStak{
	ProcInfo *pinfo;
	struct ProcStak *next;
}ProcStak;

enum{
	STAT_FR, //Foreground Running
	STAT_BR, //Background Running
	STAT_BS, //Background Stopped
	STAT_K,  //Killed
	STAT_CNT //Number of statusssses
};

//description of different states of a process
extern const char *statdesc[];

//Home Directory,Username,Machine Name
char *hDir,*uName,*mName;

//Array to store child processes
ProcStak *procstack;
int proccnt;
volatile int infg;

#endif
