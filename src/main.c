#include "main.h"

int main(int argc,char *argv[]){
	uid_t uid;
	pid_t cpid;
	struct passwd *pfile;
	int cmdnum,pipenum,inpfd,outfd,save_in,save_out,pipefd[2];
	char *instr,*tempstr_1,**cmdlist,**pipelist;
	
	//get the UserID
	uid=getuid();
	//get the password file entry
	pfile=getpwuid(uid);

	//Allocate memory/values to global strings
	hDir=NULL;
	uName=(char *)malloc(sizeof(char)*STRSIZE);
	mName=(char *)malloc(sizeof(char)*STRSIZE);
	instr=(char *)malloc(sizeof(char)*STRSIZE);

	//initialize Proc info
	initProc();

	//Check string sizes
	if(strlen(pfile->pw_name)>STRSIZE){
		fprintf(stderr,"Username too long\n");
		exit(-1);
	}
	//Copy the strings to corresponding pointers
	hDir=pwd_abs();
	strncpy(uName,pfile->pw_name,STRSIZE);
	uName[STRSIZE-1]='\0';
	if(gethostname(mName,STRSIZE)<0){
		perror("Could'nt fetch host machine name");
		exit(-1);
	}

	//signal handling
	if (signal(SIGUSR1, sig_handler) == SIG_ERR)
		fprintf(stderr,"\n"STR_RED"Error :"STR_NORM"can't catch "STR_BLU"SIGUSR1"STR_NORM"\n");
	if (signal(SIGTSTP, sig_handler) == SIG_ERR)
		fprintf(stderr,"\n"STR_RED"Error :"STR_NORM"can't catch "STR_BLU"SIGTSTP"STR_NORM"\n");
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		fprintf(stderr,"\n"STR_RED"Error :"STR_NORM"can't catch "STR_BLU"SIGINT"STR_NORM"\n");

	//Main loop
	while(142857){
		//set the fg as not running
		infg=-1;
		//handle signals sent by children
		handleProc();
		//print the shell prompt
		printPrompt();
		//read input into instr
		fgets(instr,STRSIZE,stdin);
		//remove the newline character at the end of instr
		instr[strlen(instr)-1]='\0';
		//tokenize the input based on semicolons
		cmdlist=tokenize(instr,";",0);
		cmdnum=0;
		//check if cmdlist is created properly
		if(cmdlist==NULL){
			continue;
		}

		//for each command seperated by a semicolon :
		while(cmdlist[cmdnum]!=NULL){
			//resetore inpfd
			inpfd=0;
			//split by pipes
			pipelist=tokenize(cmdlist[cmdnum],"|",0);
			pipenum=0;
			//check if pipelist is created properly
			if(pipelist==NULL){
				cmdnum++;
				continue;
			}

			//piping routine
			while(pipelist[pipenum]!=NULL){
				//next command recieves pipe
				if(pipelist[pipenum+1]!=NULL){
					pipe(pipefd);
					outfd=pipefd[1];
				}
				else{
					outfd=1;
				}
				//run the command
				runit(pipelist[pipenum],inpfd,outfd);
				//close unused fd
				if(pipelist[pipenum+1]!=NULL){
				    close(pipefd[1]);
				}
				if(inpfd!=0)
					close(inpfd);
				inpfd=pipefd[0];
				//go to next pipe
				pipenum++;
			}
			freetoken(pipelist);
			//go to the next function seperated by semicolon
			cmdnum++;
		}
		freetoken(cmdlist);
	}
	return 0;
}


void sig_handler(int signo){
	if(signo==SIGTSTP){
		if(infg>=0){
			kjob(infg,SIGSTOP);
			changeProcStat(infg,STAT_BS);
			infg=0;
		}
	}
	else if(signo==SIGINT){
		if(infg>=0){
			kjob(infg,SIGKILL);
			changeProcStat(infg,STAT_K);
			infg=0;
		}
	}
	else if(signo==SIGCHLD){
		printf("Child stopped\n");
	}
}
