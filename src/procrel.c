#include "procrel.h"

const char *statdesc[]={"Currently running","Running in background","Background process stopped","Process killed","Should never be printed"};

void initProc(void){
	procstack=NULL;
	proccnt=0;
}

ProcInfo *makeProcInfo(int index,int status,pid_t pid,const char *name){
	ProcInfo *toret=(ProcInfo *)malloc(sizeof(ProcInfo));
	toret->index=index;
	toret->status=status;
	toret->pid=pid;
	toret->name=(char *)malloc(sizeof(char)*STRSIZE);
	strncpy(toret->name,name,STRSIZE);
	toret->name[STRSIZE-1]='\0';
	return toret;
}

int addProc(int status,pid_t pid,const char *name){
	ProcStak *tmp;
	tmp=procstack;
	procstack=(ProcStak *)malloc(sizeof(ProcStak));
	procstack->pinfo=makeProcInfo(++proccnt,status,pid,name);
	procstack->next=tmp;
	if(procstack->pinfo->status==STAT_BR)
		printf("added [%d] "STR_CYN"%s"STR_NORM" %d\n",procstack->pinfo->index,procstack->pinfo->name,procstack->pinfo->pid);
	return proccnt;
}

int removeProc(int index){
	ProcStak **tmp,*tofree;
	tmp=getProc(index);
	if(tmp==NULL){
		fprintf(stderr,STR_RED"Error :"STR_NORM" No process with index %d to remove\n",index);
		if(getProcIndex((pid_t)index)>=0)
			fprintf(stderr,STR_BLU"Note :"STR_NORM" Use the ID in \'jobs\' instead of the pid of the process");
		return -1;
	}
	tofree=*tmp;	
	*tmp=tofree->next;
	free(tofree->pinfo->name);
	free(tofree->pinfo);
	return index;
}

ProcStak **getProc(int index){
	ProcStak **toret;
	toret=&procstack;
	while(*toret!=NULL&&(*toret)->pinfo->index>=index){
		if((*toret)->pinfo->index==index){
			return toret;
		}
		toret=&((*toret)->next);
	}
	return NULL;
}

int getProcIndex(pid_t pid){
	ProcStak *tmp;
	tmp=procstack;
	while(tmp!=NULL){
		if(tmp->pinfo->pid==pid)
			return tmp->pinfo->index;
		tmp=tmp->next;
	}
	return -1;
}

void jobs(ProcStak *item){
	if(item==NULL)
		return ;
	jobs(item->next);
	printf("[%d] "STR_CYN"%s"STR_NORM" [%d] : "STR_YEL"%s"STR_NORM"\n",item->pinfo->index,item->pinfo->name,item->pinfo->pid,statdesc[item->pinfo->status]);
}

int kjob(int index,int signal){
	ProcStak **tmp;
	tmp=getProc(index);
	if(tmp==NULL){
		fprintf(stderr,STR_RED"Error :"STR_NORM" kjob failed - No process with id %d found\n",index);
		if(getProcIndex((pid_t)index)>=0)
			fprintf(stderr,STR_BLU"Note :"STR_NORM" Use the ID in \'jobs\' instead of the pid of the process\n");
		return -1;
	}
	if(kill((*tmp)->pinfo->pid,signal)==0){
		printf("Sent %d to [%d] "STR_CYN"%s"STR_NORM"\n",signal,index,(*tmp)->pinfo->name);
		return index;
	}
	else{
		fprintf(stderr,STR_RED"Error :"STR_NORM" Failed to send signal %d to [%d] %s\n",signal,index,(*tmp)->pinfo->name);
		return -1;
	}
}

void overkill(void){
	ProcStak *tmp;
	pid_t pid;
	tmp=procstack;
	while(tmp!=NULL){
		pid=tmp->pinfo->pid;
		tmp=tmp->next;
		if(kill(pid,SIGKILL)!=0){
			fprintf(stderr,STR_RED"Error :"STR_NORM" Couldn't kill process with pid %d\n",pid);
		}
		else{
			removeProc(getProcIndex(pid));
		}
	}
}

void handleProc(void){
	ProcStak *tmp;
	int status,toRemove;
	pid_t pid;
	
	//Clean killed processes
	tmp=procstack;
	while(tmp!=NULL){
		if(tmp->pinfo->status==STAT_K){
			toRemove=tmp->pinfo->index;
			tmp=tmp->next;
			removeProc(toRemove);
		}
		else{
			tmp=tmp->next;
		}
	}

	//Handle signals
	while((pid=waitpid(-1,&status,WNOHANG|WCONTINUED))!=0){
		/* if(pid<0){ */
		/* 	//pid is -1 due to absence of child processes */
		/* 	if(procstack==NULL){ */
		/* 		return ; */
		/* 	} */
		/* 	fprintf(stderr,STR_RED"Fatal Error :"STR_NORM" Couldn't handle process\n"STR_BLK"waitpid"STR_NORM" exited with %d\n",pid); */
		/* 	perror("waitpid failed due to "); */
		/* 	exit(EXIT_FAILURE); */
		/* } */
		if(pid<0)
			break;
		if(getProc(getProcIndex(pid))==NULL){
			fprintf(stderr,STR_RED"Fatal Error :"STR_NORM" Couldn't get process data\n"STR_BLK"waitpid"STR_NORM" exited with %d not found\n",pid);
			exit(EXIT_FAILURE);
		}
		tmp=*getProc(getProcIndex(pid));
		if(WIFEXITED(status)){
			printf("[%d] "STR_CYN"%s"STR_NORM" Exited normally\n",tmp->pinfo->index,tmp->pinfo->name);
			changeProcStat(tmp->pinfo->index,STAT_K);
		}
		else if(WIFSIGNALED(status)){
			printf("[%d] "STR_CYN"%s"STR_NORM" Process was "STR_RED"killed"STR_NORM"\n",tmp->pinfo->index,tmp->pinfo->name);
			changeProcStat(tmp->pinfo->index,STAT_K);
		}
		else if(WIFCONTINUED(status)){
			printf("[%d] "STR_CYN"%s"STR_NORM" Resuming\n",tmp->pinfo->index,tmp->pinfo->name);
			changeProcStat(tmp->pinfo->index,STAT_FR);
		}
	}

	//Clean killed processes
	tmp=procstack;
	while(tmp!=NULL){
		if(tmp->pinfo->status==STAT_K){
			toRemove=tmp->pinfo->index;
			tmp=tmp->next;
			removeProc(toRemove);
		}
		else{
			tmp=tmp->next;
		}
	}
}

int changeProcStat(int index,int newStat){
	ProcStak **tmp;
	tmp=getProc(index);
	if(tmp==NULL)
		return -1;
	(*tmp)->pinfo->status=newStat;
	return index;
}

int fg(int index){
	ProcStak **tmp;
	int status;
	pid_t pid;
	tmp=getProc(index);
	if(tmp==NULL){
		fprintf(stderr,STR_RED"Error :"STR_NORM" No process with id "STR_GRN"%d"STR_NORM"\n",index);
		return -1;
	}
	infg=index;
	pid=(*tmp)->pinfo->pid;
	if(changeProcStat(index,STAT_FR)!=index){
		fprintf(stderr,STR_RED"Error :"STR_NORM" No process with id "STR_GRN"%d"STR_NORM"\n",index);
		return -1;		
	}
	kjob(index,SIGCONT);
	waitpid(pid,&status,WUNTRACED);
	if(WIFSTOPPED(status)){
		changeProcStat(infg,STAT_BS);
	}
	else{
		changeProcStat(infg,STAT_K);
	}
	infg=0;
	return index;
}
