#include "include.h"

void initProc(void);
ProcInfo *makeProcInfo(int,int,pid_t,const char *);
int addProc(int,pid_t,const char *);
int removeProc(int);
ProcStak **getProc(int);
int getProcIndex(pid_t);
void jobs(ProcStak *);
int kjob(int,int);
void overkill(void);
void handleProc(void);
int changeProcStat(int,int);
int fg(int);
